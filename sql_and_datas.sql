-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 27, 2015 at 04:03 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `africodes`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `CUSTOMER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(40) NOT NULL,
  `FULLNAME` varchar(50) NOT NULL,
  `CUSTOMER_PHONE` bigint(20) NOT NULL,
  `CUSTOMER_DATE` varchar(40) NOT NULL,
  `SECRET_PIN` varchar(40) DEFAULT NULL,
  `STATE_OF_RESIDENCE` varchar(20) DEFAULT NULL,
  `IMAGE_PATH` varchar(50) NOT NULL,
  PRIMARY KEY (`CUSTOMER_ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`CUSTOMER_ID`, `EMAIL`, `FULLNAME`, `CUSTOMER_PHONE`, `CUSTOMER_DATE`, `SECRET_PIN`, `STATE_OF_RESIDENCE`, `IMAGE_PATH`) VALUES
(1, 'ani@gmail.com', 'anietzzz', 70633444344, '', NULL, NULL, 'new/27-08-2015-1440690573prf.jpg'),
(3, 'femiapps@gmail.com', 'Femi Bejide', 2348034545561, '08-17-2015', '344444444444444', 'Lagos', 'images/26-08-2015-1440604456prf.jpg'),
(27, 'abb@rggg.com', 'femi', 866666, '08-17-2015', '344455555', 'Lagos', 'images/26-08-2015-1440606895prf.jpg'),
(28, 'anerr@yahoo.com', 'soji', 70455555, '08-18-2015', '888877666665', 'Ibadan', 'images/26-08-2015-1440606895prf.jpg'),
(29, 'ddd@yahoo.com', 'ade', 70546666, '08-04-2015', ' 4c85lihi3', 'Ibadan', 'images/27-08-2015-1440686334prf.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
  `TRANSACTION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(11) NOT NULL,
  `JANUARY` int(11) NOT NULL,
  `FEBUARY` int(11) NOT NULL,
  `MARCH` int(11) NOT NULL,
  `APRIL` int(11) NOT NULL,
  `MAY` int(11) NOT NULL,
  `JUNE` int(11) NOT NULL,
  PRIMARY KEY (`TRANSACTION_ID`),
  KEY `CUSTOMER_ID` (`CUSTOMER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`TRANSACTION_ID`, `CUSTOMER_ID`, `JANUARY`, `FEBUARY`, `MARCH`, `APRIL`, `MAY`, `JUNE`) VALUES
(1, 3, 20, 67, 18, 6, 80, 78);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(20) NOT NULL,
  `PASSWORD` varchar(20) NOT NULL,
  `EMAIL` varchar(40) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`USER_ID`, `USERNAME`, `PASSWORD`, `EMAIL`) VALUES
(1, 'ani@gmail.com', '123456', 'ani@gmail.com');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `customers` (`CUSTOMER_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
