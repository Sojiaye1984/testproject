<?php
require_once("functions.php");
session_start();
/*if (!isset($_SESSION['username'])){  
header("location:login.php");
die();
}*/
confirm_logged_in();



?>

<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from thevectorlab.net/flatlab/boxed_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Jun 2015 14:30:10 GMT -->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.html">

    <title>Form Validation</title>

    <!-- Bootstrap core CSS -->
     <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
      <link href="assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
     <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
      <!--right slidebar-->
      <link href="css/slidebars.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script type="text/javascript" src="assets/bootstrap-fileupload/bootstrap-fileupload.js"></script>
    <link href="assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet">
      <link href="css/gallery.css" rel="stylesheet" />
       <script src="js/jquery.js"></script>
  

      <link href="css/spin.css" rel="stylesheet" />
    <link href="css/jquery.steps.css" rel="stylesheet" />
    <link href="css/style-responsive.css" rel="stylesheet" />
      <link href="css/profile.css" rel="stylesheet" />
      
      <script type="text/javascript">
$(document).ready(function(e){
$('#formsubmit').on('change',(function(e){
//$('#statusloading').show(); 
e.preventDefault();
                    $.ajax({
                    type: "POST",
                    url: "ajax.php",
                    data: new FormData(this),
                    contentType:false,
                    cache:false,
                    processData:false,
                    success: function(html){

                    if ( html == "failed") {
                    
                     //$('#statusloading').hide();                   
                     $('#upload_file').val(null);
                     console.log(html);
                    } else {
                      
                     
                    $('#imggg').attr("src",html);
                    //$('#statusloading').hide();
                    $('#upload_file').val(null);
                    console.log(html);
                    };  
                    


                    }

                    });
}));
});
      </script>

    <script type="text/javascript">
$(document).ready(function(){
$("#email").keyup(function() {
var name = $('#email').val();
if(name=="")
{
$("#disp1").html("");
}
else
{
$.ajax({
type: "POST",
url: "user_check.php",
data: "name="+ name ,
success: function(html){
$("#disp1").html(html);
}
});
return false;
}
});
});
</script>  
<script type="text/javascript">

$(document).ready(function(){
$("#but1").click(function(){  
$.ajax({
type: "POST", 
url: "get_customers.php",
success: function(res){
console.log(res);

$('#sec4').append( $(res) );
}
});
return false;
});
});

</script>

<script type="text/javascript">
$(document).ready(function(){
$(function(){
var text1 = "abcdefghijklmnop0123456789";
var ranNum = " ";
for (var i = 0; i < 9; i++) {
    ranNum += text1.charAt(Math.floor(Math.random()*text1.length));

  }  

$('#secret_pin').val(ranNum);


});
});



</script>


<script type="text/javascript">
$(document).ready(function(){
$('#form_submit2').on('submit',(function(e){
e.preventDefault();

$.ajax({
type: "POST",
url: "ajax1.php",
//data: "fullname="+fullname+"&email="+email+"&date_customer="+date_customer+"&select_state="+select_state+"&secret_pin="+secret_pin+"&phone="+phone,
 data: new FormData(this),
 contentType:false,
  cache:false,
  processData:false,
success: function(html){
console.log(html);
if ( html == "success") {
console.log(html);
$("#fullname").val(null);
  $("#email").val(null);
  $("#date_customer").val(null);
  $("#select_state").val(null);
  $("#secret_pin").val(null);
$("#phone").val(null);
  
}
else if( html == "failed"){ 
 console.log(html);  
}  
}
});
return false;
}));
});
</script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="">



      <!-- header start -->
        <header class="header blue-bg">
              <div class="container ">
                  <div class="sidebar-toggle-box">
                      <div data-original-title="Toggle Navigation" data-placement="right" class="fa fa-reorder tooltips"></div>
                  </div>
                  <!--logo start-->
                  <a href="index-2.html" class="logo" >AFRICA<span style="color:#FCB322;">CODES</span></a>
                  <!--logo end-->
                  <div class="nav notify-row" id="top_menu">
                    <!--  notification start -->
                    
                  </div>
                  <div class="top-nav ">
                      <ul class="nav pull-right top-menu">
                          <li>
                              <input type="text" class="form-control search" placeholder="Search">
                          </li>
                          <!-- user login dropdown start-->
                          <li class="dropdown">
                              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                  <img alt="" src="img/images.png" style="width: 30px;height: 30px;">
                                  <span class="username"><?php echo $_SESSION['USERNAME']; ?></span>
                                  <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu extended logout">
                                  <div class="log-arrow-up"></div>
                                  
                                  <li><a href="logout.php?logG=1"><i class="fa fa-key"></i> Log Out</a></li>
                              </ul>
                          </li>
                          
                          <!-- user login dropdown end -->
                      </ul>
                  </div>
              </div>
        </header>
    <!-- header end -->

    <section id="main-content" style="margin-left:0px;">
        <!-- <div class="first_page bg_grey" id="sec_1"> -->
          <div class="container">
            <div id="srch_container"  style="margin-top: 100px;">
              <div class="row">
               
                <div class="theme-config" style="">
                  <div class="theme-config-box">
                      <div class="spin-icon">
                          <i class="fa fa-cogs fa-spin"></i>
                      </div>
                      <div class="skin-setttings">
                          <div class="title">Instructions</div>
                             <div class="skin-setttings" style="margin-left:0px;">
                          
                                  <div class="setings-item" style="padding:10px;font-size:12px;">
                                    <p>1. Upload profile picture using AJAX (into your images folder)</p>
                                    <p>2. Bind to database table 'Customers'</p>
                                    <p>3. Create customer in format:<br>
                                      - 'Femi Bejide, +2348034545561, femiapps@gmail.com, Lagos, [sysdate], [Auto Generated: Random]', validating the following in the customers registration form;<br>
                                      - date (no future date)<br>
                                      - email (*@.com) <br>
                                      - Email: Email exists in db (test with email in DB) <br>
                                      - Phone: No special characters (only number and +)</p>
                                      
                                    <p>Submit: Save to Customer table, Load Customer Table (Use Ajax)</p>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>
              
                  <span><a href="signin.php"><i class="fa  fa-angle-double-left" style=" font-size: 25px;color: #1ab394;"></i></a></span>
                <div class="col-lg-12" style="    margin-top: 10px;">
                
                    <div class="profile-nav col-lg-4">
                      <section class="panel" style=" height: 420px;">
                          <div class="user-heading round">
                              <a href="#">
                                  <img id="imggg" src="" alt="choose a picture">
                              </a>
                             
                               
                          </div>
                          
                          <ul class="nav nav-pills nav-stacked">
                            
                              <li ><form id="formsubmit" name="formsubmit">    <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                <span class="btn btn-white btn-file" style="width: 215px;margin-left: 8px;margin-top: 7px;">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" class="default" name="upload_file" id="upload_file">
                                                </span>
                                                  <span class="fileupload-preview" style="margin-left:5px;"></span>
                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                              </div>
                                              </form>
                                               </li>
                                             
                              <li ><a class="btn" id="but1" class="clicknow" >View List</a></li>
                              <li ><a href="dashboard.php" class="btn ">Proceed</a></li>
                          </ul>
                          
                      </section>
                    </div>
                    <form class="form-horizontal" role="form" id="form_submit2"> 
                    <div class="col-lg-8">
                      <section class="panel" id="sec1" style=" height: 420px;">
                        <header class="panel-heading" style="padding:10px;" >
                          <h4>Customer Registration</h4>
                        </header>
                        <div class="panel-body" style="padding:20px;">
                            
                              
                                      
                                      <div class="form-group">
                                          <label class="col-lg-3 control-label">Full Name</label>
                                          <div class="col-lg-9">
                                              <input type="text" required id="fullname" name="fullname" class="form-control" placeholder="">
                                          </div>
                                      </div>
                                      <div class="form-group" style="margin-bottom:7px;">
                                          <label class="col-lg-3 control-label"> State of Residence</label>
                                          <div class="col-lg-9">
                                              <select class="form-control" required style="width:100%;" id="select_state" name="select_state">
                                                <option selected>Choose</option>
                                                <option>Lagos</option>
                                                <option>Ibadan</option>
                                                <option>Abuja</option>
                                              </select>
                                          </div>
                                      </div>
                                       <div class="form-group">
                                          <label class="col-lg-3 control-label">Phone</label>
                                          <div class="col-lg-9">
                                              <input type="number" required class="form-control" id="phone" name="phone" placeholder="phone">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                         <label class="col-lg-3 control-label">Email </label>
                                          <div class="col-lg-9">
                                              <input type="email" style="width: 245px;" required class="form-control" id="email" name="email" placeholder="">
                                               <p id="disp1" style="padding: 5px;margin-left: 247px;margin-top: -32px;"></p>
                                          </div>
                                      </div>

                                        <div class="form-group">
                                                  <label class="control-label col-lg-3">Date</label>
                                                  <div class="col-lg-9">
                                                      <input required class="form-control form-control-inline input-medium default-date-picker" name="date_customer" id="date_customer" size="16" type="text" value="" />
                                                  </div>
                                              </div>
                                      <div class="form-group">
                                          <label class="col-lg-3 control-label">Secret PIN</label>
                                          <div class="col-lg-9">
                                              <input required type="text" name="secret_pin" class="form-control" placeholder="" id="secret_pin">
                                          </div>
                                      </div>
                                      
                                      
                                      
                                      <button class="btn btn-info" id="t_submit" type="submit" style=" float: right; margin-left: 5px;">Submit</button> 
                                     <!-- <input type="submit"  id ="buttn" data-toggle="modal" class="finish btn btn-info" value="Submit"> --> 
                                     <input type="button" class="finish btn btn-danger" value="Cancel">
                            </form>
                          </div>
                      </section>
                      <section class="panel" id="sec2" style="display:none; height: 420px;">
                                                               <div class="panel-body" id="sec4">
                                                                  </div>
                                          <button class="btn btn-info" id="but2" type="button" style=" position: absolute; right: 34px;top: 375px; ">Back</button>
                       </section>
                    </div>
                </div>
              </div>
            </div>
          <!-- </div> -->
         <!-- modal start -->
         <div class="col-lg-8">
           
         </div>
                     
                                                       
                                    <!-- modal end -->
        </div>
    </section>

    
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script class="include" type="text/javascript" src="js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="js/bootstrap.min.js"></script>
   <!--  // <script src="http://localhost/wotaman/pub/assets/fancybox/source/jquery.fancybox.js"></script> -->
    <script src="js/jquery.scrollTo.min.js"></script>
    <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
   
   
    
 
    <script src="js/respond.min.js" ></script>
    <!--right slidebar-->
      <script type="text/javascript">
          $('.spin-icon').click(function () {
            $(".theme-config-box").toggleClass("show");
        });
        
      // replace the form
      $('#but1').click(function(){

      $("#sec1").hide() 
      // return the element
    $('#sec2').show();
    return false;
    
    
    });
       $('#but2').click(function(){

     // $("#sec2").hide()
      $('#sec2').hide();
       $('#sec4').html("");
      
      // return the element
    return $('#sec1').show();
    return false;
   
    
    });

    </script>
  
<script type="text/javascript">

/*$('#date_customer').datepicker({

format:'mm/dd/yyy',
startDate: ''

});*/

</script>

    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>
  <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
   <script src="js/advanced-form-components.js"></script>

  </body>

</html>